﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="frmRegistro.aspx.cs" Inherits="ControlRegistro.frmRegistro" %>

<%@ Register Assembly="DevExpress.Web.v21.1, Version=21.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <asp:HiddenField runat="server" ID="hIdEmpleado" Value="0" />
        <div class="col-md-12">
            <dx:ASPxFormLayout runat="server" ID="OptionsFormLayout">
                <Items>
                    <dx:LayoutGroup Caption="Registro" GroupBoxDecoration="HeadingLine">
                        <Items>
                            <dx:LayoutItem Caption="Primer Nombre">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                        <dx:ASPxTextBox runat="server" ID="txtPrimerNombre" CssClass="form-control" Width="100%" MaxLength="20">
                                            <ValidationSettings ErrorTextPosition="Bottom" ErrorDisplayMode="Text" ValidationGroup="MyGroup">
                                                <ErrorFrameStyle Font-Size="Smaller" />
                                                <RequiredField IsRequired="True" ErrorText="Este Campo es Requerido" />
                                                <RegularExpression ValidationExpression="^[a-zA-Z]*$" ErrorText="No se permite los caracteres" />
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>

                            <dx:LayoutItem Caption="Segundo Nombre">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                        <dx:ASPxTextBox runat="server" ID="txtSegundoNombre" CssClass="form-control" Width="100%" MaxLength="20">
                                            <ValidationSettings ErrorTextPosition="Bottom" ErrorDisplayMode="Text" ValidationGroup="MyGroup">
                                                <ErrorFrameStyle Font-Size="Smaller" />
                                                <RegularExpression ValidationExpression="^[a-zA-Z]*$" ErrorText="No se permite los caracteres" />
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>

                            <dx:LayoutItem Caption="Primer Apellido">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                        <dx:ASPxTextBox runat="server" ID="txtPrimerApellido" CssClass="form-control" Width="100%" MaxLength="20">
                                            <ValidationSettings ErrorTextPosition="Bottom" ErrorDisplayMode="Text" ValidationGroup="MyGroup">
                                                <ErrorFrameStyle Font-Size="Smaller" />
                                                <RequiredField IsRequired="True" ErrorText="Este Campo es Requerido" />
                                                <RegularExpression ValidationExpression="^[a-zA-Z]*$" ErrorText="No se permite los caracteres" />
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>

                            <dx:LayoutItem Caption="Segundo Apellido">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                        <dx:ASPxTextBox runat="server" ID="txtSegundoApellido" CssClass="form-control" Width="100%" MaxLength="20">
                                            <ValidationSettings ErrorTextPosition="Bottom" ErrorDisplayMode="Text" ValidationGroup="MyGroup">
                                                <ErrorFrameStyle Font-Size="Smaller" />
                                                <RegularExpression ValidationExpression="^[a-zA-Z]*$" ErrorText="No se permite los caracteres" />
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>

                            <dx:LayoutItem Caption="Tipo Documento">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                        <dx:ASPxComboBox runat="server" ID="drpTipoIdentificacion" Width="100%" SelectedIndex="0" CssClass="form-control">
                                        </dx:ASPxComboBox>
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>

                            <dx:LayoutItem Caption="Identificación">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                        <dx:ASPxTextBox runat="server" ID="txtIdentificacion" CssClass="form-control" Width="100%" MaxLength="20">
                                            <ValidationSettings ErrorTextPosition="Bottom" ErrorDisplayMode="Text" ValidationGroup="MyGroup">
                                                <ErrorFrameStyle Font-Size="Smaller" />
                                                <RequiredField IsRequired="True" ErrorText="Este Campo es Requerido" />
                                                <RegularExpression ValidationExpression="^[a-zA-Z0-9-]*$" ErrorText="Numero Invalido" />
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>

                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>

                            <dx:LayoutItem Caption="Pais Trabajo">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                        <dx:ASPxComboBox runat="server" ID="drpPais" Width="100%" SelectedIndex="0" CssClass="form-control">
                                        </dx:ASPxComboBox>
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>

                            <dx:LayoutItem Caption="Email">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                        <table>
                                            <tr>
                                                <td>
                                                    <dx:ASPxTextBox ID="txtEmail" runat="server" MaxLength="300" Width="100%" CssClass="form-control" Enabled="false">
                                                        <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="Text" ErrorTextPosition="Bottom" SetFocusOnError="true" ValidationGroup="MyGroup">
                                                            <ErrorFrameStyle Font-Size="Smaller" />
                                                            <RequiredField IsRequired="True" ErrorText="Este Campo es Requerido" />
                                                            <RegularExpression ValidationExpression="^\w+([-+.'%]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$"
                                                                ErrorText="Email Invalido" />
                                                        </ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td style="width: 10px"></td>
                                                <td>
                                                    <dx:ASPxButton ID="bttGenerarEmail" runat="server" CssClass="btn btn-success" Text="Generar Email" OnClick="bttGenerarEmail_Click" CausesValidation="false"></dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>


                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                            <dx:LayoutItem Caption="Fecha Ingreso">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                        <dx:ASPxCalendar ID="txtFecha" runat="server" />
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>

                            <dx:LayoutItem Caption="Área">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                        <dx:ASPxComboBox runat="server" ID="drpArea" Width="100%" SelectedIndex="0" CssClass="form-control">
                                        </dx:ASPxComboBox>
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                            <dx:LayoutItem HorizontalAlign="Right" ShowCaption="False">
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer runat="server">
                                        <dx:ASPxButton ID="bttGiardar" runat="server" Text="Guardar" CssClass="btn btn-success" OnClick="bttGiardar_Click" CausesValidation="true" ValidationGroup="MyGroup">
                                        </dx:ASPxButton>
                                    </dx:LayoutItemNestedControlContainer>
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                        </Items>
                    </dx:LayoutGroup>
                </Items>
            </dx:ASPxFormLayout>
        </div>

    </div>
    <div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="upModal" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header" style="background:rgba(38 29 110)">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">
                                <asp:Label ID="lblModalTitle" runat="server" Text="" ForeColor="White"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <asp:Label ID="lblModalBody" runat="server" Text=""></asp:Label>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-success" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
