﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CIDENET.REGISTRO.DATOS;
using DevExpress.Web;

namespace ControlRegistro
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            ListarEmpleados();

        }
        private void ListarEmpleados()
        {
            EmpleadoDB objEmpleado = new EmpleadoDB();
            DataTable dttEmpleado = objEmpleado.ListarEmpleados();
            dtgEmpleado.DataSource = dttEmpleado;
            dtgEmpleado.DataBind();
        }

        protected void dtgEmpleado_SelectionChanged(object sender, EventArgs e)
        {
            ASPxGridView grid = sender as ASPxGridView;
            var data = grid.GetSelectedFieldValues("IdEmpleado");
            string IdEmpleado = data[0].ToString();
            DevExpress.Web.ASPxWebControl.RedirectOnCallback("frmRegistro.aspx?IdEmpleado=" + IdEmpleado);
        }

        protected void dtgEmpleado_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            e.Cancel = true;
            string IdEmpleado = e.Values["IdEmpleado"].ToString();
            string PrimerNombre = e.Values["PrimerNombre"].ToString();
            string SegundoNombre = e.Values["SegundoNombre"].ToString();
            string PrimerApellido = e.Values["PrimerApellido"].ToString();
            string SegundoApellido = e.Values["SegundoApellido"].ToString();
            EmpleadoDB objEmpleado = new EmpleadoDB();
            objEmpleado.RegistrarLog("Eliminar Informacion", "Se elimina informacion del empleado: " + PrimerNombre + " " + SegundoNombre + " " + PrimerApellido + " " + SegundoApellido);
            objEmpleado.EliminarEmpleado(IdEmpleado);
            ListarEmpleados();
        }

        protected void dtgEmpleado_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            ASPxGridView grid = sender as ASPxGridView;
            string IdEmpleado = grid.GetRowValues(e.VisibleIndex, "IdEmpleado").ToString();
            DevExpress.Web.ASPxWebControl.RedirectOnCallback("frmRegistro.aspx?IdEmpleado=" + IdEmpleado);
        }
    }
}