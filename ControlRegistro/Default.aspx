﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ControlRegistro._Default" %>

<%@ Register Assembly="DevExpress.Web.v21.1, Version=21.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="container">
        <div class="row">
            <dx:ASPxGridView ID="dtgEmpleado" ClientInstanceName="dtgEmpleado" runat="server" KeyFieldName="IdEmpleado" OnSelectionChanged="dtgEmpleado_SelectionChanged" OnRowDeleting="dtgEmpleado_RowDeleting"
                Width="100%" AutoGenerateColumns="False" OnCustomButtonCallback="dtgEmpleado_CustomButtonCallback" EnableCallBacks="true" Theme="PlasticBlue">
                <Columns>
                    <dx:GridViewCommandColumn VisibleIndex="1">
                        <CustomButtons>
                            <dx:GridViewCommandColumnCustomButton ID="bttEditar" Text="Editar" />
                        </CustomButtons>
                    </dx:GridViewCommandColumn>
                    <dx:GridViewCommandColumn ShowDeleteButton="True" />

                    <dx:GridViewDataTextColumn FieldName="IdEmpleado" Width="200px" Visible="false" />
                    <dx:GridViewDataTextColumn FieldName="PrimerNombre" Width="200px" />
                    <dx:GridViewDataTextColumn FieldName="SegundoNombre" />
                    <dx:GridViewDataTextColumn FieldName="PrimerApellido" Width="100px" />
                    <dx:GridViewDataTextColumn FieldName="SegundoApellido" Width="100px" />
                    <dx:GridViewDataTextColumn FieldName="TipoIdentificacion" Width="100px" />
                    <dx:GridViewDataTextColumn FieldName="Identificacion" Width="100px" />
                    <dx:GridViewDataTextColumn FieldName="Email" Width="100px" />
                    <dx:GridViewDataTextColumn FieldName="Pais" Width="100px" />
                    <dx:GridViewDataTextColumn FieldName="Estado" Width="100px" />
                </Columns>
                <Settings ShowFilterRow="true" />


                <SettingsText ConfirmDelete="¿Está seguro de eliminar el empleado?" />
                <SettingsPager>
                    <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                </SettingsPager>
                <SettingsDataSecurity AllowInsert="false" AllowDelete="true" />
                <SettingsBehavior AllowSelectByRowClick="True" AllowSelectSingleRowOnly="True" ProcessSelectionChangedOnServer="true" ConfirmDelete="true" />
            </dx:ASPxGridView>

        </div>
    </div>


</asp:Content>
