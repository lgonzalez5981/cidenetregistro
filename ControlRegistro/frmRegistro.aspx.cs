﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CIDENET.REGISTRO.DATOS;

namespace ControlRegistro
{
    public partial class frmRegistro : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ListarTipoIdentificacion();
                ListarPais();
                ListarArea();
                if (Request.QueryString["IdEmpleado"] != null)
                {
                    string IdEmpleado = Request.QueryString["IdEmpleado"].ToString();
                    ListarInformacionEmpleado(IdEmpleado);
                }
                else
                {
                    txtFecha.SelectedDate = DateTime.Now;
                }

            }
        }

        private void ListarInformacionEmpleado(string IdEmpleado)
        {
            EmpleadoDB objEmpleado = new EmpleadoDB();
            DataTable dttEmpleado = objEmpleado.ListarInformacionEmpleado(IdEmpleado);
            if (dttEmpleado != null)
            {
                hIdEmpleado.Value = IdEmpleado;
                string PrimerNombre = dttEmpleado.Rows[0]["PrimerNombre"].ToString();
                txtPrimerNombre.Text = PrimerNombre;
                string SegundoNombre = dttEmpleado.Rows[0]["SegundoNombre"].ToString();
                txtSegundoNombre.Text = SegundoNombre;
                string PrimerApellido = dttEmpleado.Rows[0]["PrimerApellido"].ToString();
                txtPrimerApellido.Text = PrimerApellido;
                string SegundoApellido = dttEmpleado.Rows[0]["SegundoApellido"].ToString();
                txtSegundoApellido.Text = SegundoApellido;
                string IdTipoIdentificacion = dttEmpleado.Rows[0]["IdTipoIdentificacion"].ToString();
                drpTipoIdentificacion.Value = IdTipoIdentificacion;
                string Identificacion = dttEmpleado.Rows[0]["Identificacion"].ToString();
                txtIdentificacion.Text = Identificacion;
                string CodigoPais = dttEmpleado.Rows[0]["CodigoPais"].ToString();
                drpPais.Value = CodigoPais;
                string Email = dttEmpleado.Rows[0]["Email"].ToString();
                txtEmail.Text = Email;
                DateTime FechaIngreso = Convert.ToDateTime(dttEmpleado.Rows[0]["FechaIngreso"].ToString());
                txtFecha.Value = FechaIngreso;
                txtFecha.Enabled = false;
                string IdArea = dttEmpleado.Rows[0]["IdArea"].ToString();
                drpArea.Value = IdArea;
            }
        }

        private void ListarTipoIdentificacion()
        {
            EmpleadoDB objEmpleado = new EmpleadoDB();
            DataTable dttTipoDocumento = objEmpleado.ListarTipoIdentificacion();
            drpTipoIdentificacion.DataSource = dttTipoDocumento;
            drpTipoIdentificacion.TextField = "TipoIdentificacion";
            drpTipoIdentificacion.ValueField = "IdTipoIdentificacion";
            drpTipoIdentificacion.DataBind();
        }
        private void ListarPais()
        {
            EmpleadoDB objEmpleado = new EmpleadoDB();
            DataTable dttPais = objEmpleado.ListarPais();
            drpPais.DataSource = dttPais;
            drpPais.TextField = "Pais";
            drpPais.ValueField = "CodigoPais";
            drpPais.DataBind();

        }
        private void ListarArea()
        {
            EmpleadoDB objEmpleado = new EmpleadoDB();
            DataTable dttArea = objEmpleado.ListarArea();
            drpArea.DataSource = dttArea;
            drpArea.TextField = "Area";
            drpArea.ValueField = "IdArea";
            drpArea.DataBind();
        }

        protected void bttGenerarEmail_Click(object sender, EventArgs e)
        {
            EmpleadoDB objEmpleado = new EmpleadoDB();
            string strEmail = "";
            string strPrimerNombre = txtPrimerNombre.Text;
            string strPrimerApellido = txtPrimerApellido.Text;
            string strPais = drpPais.Value.ToString();

            strEmail = String.Concat(strPrimerNombre.Where(c => !Char.IsWhiteSpace(c))) + "." + String.Concat(strPrimerApellido.Where(c => !Char.IsWhiteSpace(c))) + "@cidenet.com." + strPais.ToLower();
            bool CorreoValido = false;
            int Contador = 0;
            string EmailOriginal = strEmail;
            while (!CorreoValido)
            {

                DataTable dttValido = objEmpleado.ValidarCorreo(strEmail);
                if (dttValido == null)
                {
                    CorreoValido = true;
                }
                else
                {
                    Contador++;
                    string[] subs = EmailOriginal.Split('@');
                    string strNuevoCorreo = subs[0] + Contador.ToString();
                    strEmail = strNuevoCorreo + "@" + subs[1];
                }
            }
            txtEmail.Text = strEmail;
        }

        protected void bttGiardar_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                DateTime dtmFechaIngreso = txtFecha.SelectedDate;
                if (dtmFechaIngreso < DateTime.Now)
                {

                    if ((DateTime.Now.Month - dtmFechaIngreso.Month) >= 0 && (DateTime.Now.Month - dtmFechaIngreso.Month) <= 1 && dtmFechaIngreso.Year == DateTime.Now.Year)
                    {
                        EmpleadoDB objEmpleado = new EmpleadoDB();
                        string IdEmpleado = hIdEmpleado.Value;
                        string PrimerNombre = txtPrimerNombre.Text;
                        string Segundonombre = txtSegundoNombre.Text;
                        string PrimerApellido = txtPrimerApellido.Text;
                        string SegundoApellido = txtSegundoApellido.Text;
                        string TipoIdentificacion = drpTipoIdentificacion.Value.ToString();
                        string Identificacion = txtIdentificacion.Text;
                        string CodigoPais = drpPais.Value.ToString();

                        if (hIdEmpleado.Value != "0")
                        {
                            DataTable dttEmpleado = objEmpleado.ListarInformacionEmpleado(hIdEmpleado.Value);
                            string PrimerNombreAnterior = dttEmpleado.Rows[0]["PrimerNombre"].ToString();
                            string PrimerApellidoAnterior = dttEmpleado.Rows[0]["PrimerApellido"].ToString();
                            if (PrimerNombre != PrimerNombreAnterior || PrimerApellido != PrimerApellidoAnterior)
                            {
                                bttGenerarEmail_Click(null, null);
                            }

                        }


                        string Email = txtEmail.Text;
                        DateTime FechaIngreso = dtmFechaIngreso;
                        string IdArea = drpArea.Value.ToString();
                        objEmpleado.GuardarUsuario(IdEmpleado, PrimerNombre, Segundonombre, PrimerApellido, SegundoApellido, TipoIdentificacion, Identificacion, CodigoPais, Email, FechaIngreso, IdArea);
                        objEmpleado.RegistrarLog("Guardar Informacion", "Se guarda informacion del empleado: " + PrimerNombre + " " + Segundonombre + " " + PrimerApellido + " " + SegundoApellido);
                        Limpiar();
                        lblModalTitle.Text = "Exito";
                        lblModalBody.Text = "Empleado guardado correctamente";
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
                        upModal.Update();
                    }
                    else
                    {
                        if (hIdEmpleado.Value != "0")
                        {
                            EmpleadoDB objEmpleado = new EmpleadoDB();
                            string IdEmpleado = hIdEmpleado.Value;
                            string PrimerNombre = txtPrimerNombre.Text;
                            string Segundonombre = txtSegundoNombre.Text;
                            string PrimerApellido = txtPrimerApellido.Text;
                            string SegundoApellido = txtSegundoApellido.Text;
                            string TipoIdentificacion = drpTipoIdentificacion.Value.ToString();
                            string Identificacion = txtIdentificacion.Text;
                            string CodigoPais = drpPais.Value.ToString();

                            bttGenerarEmail_Click(null, null);
                            string Email = txtEmail.Text;
                            DateTime FechaIngreso = dtmFechaIngreso;
                            string IdArea = drpArea.Value.ToString();
                            objEmpleado.GuardarUsuario(IdEmpleado, PrimerNombre, Segundonombre, PrimerApellido, SegundoApellido, TipoIdentificacion, Identificacion, CodigoPais, Email, FechaIngreso, IdArea);
                            objEmpleado.RegistrarLog("Guardar Informacion", "Se guarda informacion del empleado: " + PrimerNombre + " " + Segundonombre + " " + PrimerApellido + " " + SegundoApellido);
                            Limpiar();
                            lblModalTitle.Text = "Exito";
                            lblModalBody.Text = "Empleado guardado correctamente";
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
                            upModal.Update();
                        }
                        else
                        {
                            lblModalTitle.Text = "Alerta";
                            lblModalBody.Text = "No puede seleccionar una fecha inferior a 1 mes";
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
                            upModal.Update();
                        }

                    }
                }
                else
                {
                    lblModalTitle.Text = "Alerta";
                    lblModalBody.Text = "No puede seleccionar una fecha  mayor a la actual";
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
                    upModal.Update();
                }
            }


        }
        private void Limpiar()
        {
            txtEmail.Text = "";
            txtIdentificacion.Text = "";
            txtPrimerApellido.Text = "";
            txtPrimerNombre.Text = "";
            txtSegundoApellido.Text = "";
            txtSegundoNombre.Text = "";
            hIdEmpleado.Value = "0";
            txtFecha.Enabled = true;
            txtFecha.SelectedDate = DateTime.Now;
        }
    }
}