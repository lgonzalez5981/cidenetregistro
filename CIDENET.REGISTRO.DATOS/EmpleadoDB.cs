﻿using AccesoDatos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIDENET.REGISTRO.DATOS
{
    public class EmpleadoDB
    {
        public DataTable ListarTipoIdentificacion()
        {
            DataTable dataTable;
            Datos datos = new Datos();


            try
            {
                DataSet dataSet = datos.EjecutarSP("P_ListarTipoIdentificacion", null);
                if (dataSet.Tables[0].Rows.Count > 0)
                {
                    dataTable = dataSet.Tables[0];
                }
                else
                {
                    dataTable = null;
                }
                return dataTable;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public DataTable ListarPais()
        {
            DataTable dataTable;
            Datos datos = new Datos();


            try
            {
                DataSet dataSet = datos.EjecutarSP("P_ListarPais", null);
                if (dataSet.Tables[0].Rows.Count > 0)
                {
                    dataTable = dataSet.Tables[0];
                }
                else
                {
                    dataTable = null;
                }
                return dataTable;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public DataTable ListarArea()
        {
            DataTable dataTable;
            Datos datos = new Datos();


            try
            {
                DataSet dataSet = datos.EjecutarSP("P_ListarArea", null);
                if (dataSet.Tables[0].Rows.Count > 0)
                {
                    dataTable = dataSet.Tables[0];
                }
                else
                {
                    dataTable = null;
                }
                return dataTable;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public DataTable ValidarCorreo(string Email)
        {
            DataTable dataTable;
            Datos datos = new Datos();
            SqlParameter[] sqlParameters1 = new SqlParameter[] { new SqlParameter("@CORREO", Email)
            };

            try
            {
                DataSet dataSet = datos.EjecutarSP("P_ValidarCorreo", sqlParameters1);
                if (dataSet.Tables[0].Rows.Count > 0)
                {
                    dataTable = dataSet.Tables[0];
                }
                else
                {
                    dataTable = null;
                }
                return dataTable;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void GuardarUsuario(string IdEmpleado,string PrimerNombre, string SegundoNombre, string PrimerApellido, string SegundoApellido, string IdTipoIdentificacion, string Identificacion, string CodigoPais, string Email, DateTime FechaIngreso, string IdArea)
        {
            DataTable dataTable;
            Datos datos = new Datos();
            SqlParameter[] sqlParameters1 = new SqlParameter[] { new SqlParameter("@ID_EMPLEADO", IdEmpleado)
                                                                ,new SqlParameter("@PRIMER_NOMBRE", PrimerNombre)
                                                                ,new SqlParameter("@SEGUNDO_NOMBRE", SegundoNombre)
                                                                ,new SqlParameter("@PRIMER_APELLIDO", PrimerApellido)
                                                                ,new SqlParameter("@SEGUNDO_APELLIDO", SegundoApellido)
                                                                ,new SqlParameter("@ID_TIPO_IDENTIFICACION", IdTipoIdentificacion)
                                                                ,new SqlParameter("@IDENTIFICACION", Identificacion)
                                                                ,new SqlParameter("@CODIGO_PAIS", CodigoPais)
                                                                ,new SqlParameter("@EMAIL", Email)
                                                                ,new SqlParameter("@FECHA_INGRESO", FechaIngreso)
                                                                ,new SqlParameter("@ID_AREA", IdArea)
            };

            try
            {
                DataSet dataSet = datos.EjecutarSP("P_GuardarEmpleado", sqlParameters1);

            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public DataTable ListarEmpleados()
        {
            DataTable dataTable;
            Datos datos = new Datos();

            try
            {
                DataSet dataSet = datos.EjecutarSP("p_ListarEmpleado", null);
                if (dataSet.Tables[0].Rows.Count > 0)
                {
                    dataTable = dataSet.Tables[0];
                }
                else
                {
                    dataTable = null;
                }
                return dataTable;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public DataTable ListarInformacionEmpleado(string IdEmpleado)
        {
            DataTable dataTable;
            Datos datos = new Datos();
            SqlParameter[] sqlParameters1 = new SqlParameter[] { new SqlParameter("@ID_EMPLEADO", IdEmpleado)
            };
            try
            {
                DataSet dataSet = datos.EjecutarSP("p_ListarInformacionEmpleado", sqlParameters1);
                if (dataSet.Tables[0].Rows.Count > 0)
                {
                    dataTable = dataSet.Tables[0];
                }
                else
                {
                    dataTable = null;
                }
                return dataTable;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void EliminarEmpleado(string IdEmpleado)
        {
            DataTable dataTable;
            Datos datos = new Datos();
            SqlParameter[] sqlParameters1 = new SqlParameter[] { new SqlParameter("@ID_EMPLEADO", IdEmpleado)
            };
            try
            {
                DataSet dataSet = datos.EjecutarSP("p_EliminarEmpleado", sqlParameters1);

            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void RegistrarLog(string Accion, string Detalle)
        {
            DataTable dataTable;
            Datos datos = new Datos();
            SqlParameter[] sqlParameters1 = new SqlParameter[] { new SqlParameter("@ACCION", Accion)
                                                                 ,new SqlParameter("@DETALLE_ACCION", Detalle)
            };
            try
            {
                DataSet dataSet = datos.EjecutarSP("P_RegistrarLog", sqlParameters1);

            }
            catch (Exception e)
            {
                throw e;
            }
        }
        //public DataTable ObtenerInformacionUsuario(string strIdentificacion, string strContrasena)
        //{
        //    DataTable dataTable;

        //    SqlParameter[] sqlParameters1 = new SqlParameter[] { new SqlParameter(SP_CONSULTA_PERSONA.IDENTIFICACION, strIdentificacion)
        //                                                        ,new SqlParameter(SP_CONSULTA_PERSONA.CONTRASENA, strContrasena)
        //    };
        //    Datos datos = new Datos();


        //    try
        //    {
        //        DataSet dataSet = datos.EjecutarSP(SP_CONSULTA_PERSONA.NOMBRE_SP, sqlParameters1);
        //        if (dataSet.Tables[0].Rows.Count > 0)
        //        {
        //            dataTable = dataSet.Tables[0];
        //        }
        //        else
        //        {
        //            dataTable = null;
        //        }
        //        return dataTable;
        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //}
    }
}
